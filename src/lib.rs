use std::error::Error;
use std::{fmt, thread, fs, env, cmp, time};
use std::io::{BufReader, BufRead, Write};
use std::os::unix::net::{UnixListener, UnixStream};
use std::collections::{HashMap, HashSet};
use std::time::SystemTime;
use std::sync::{Arc, Mutex};
use bufstream::BufStream;
use threadpool::ThreadPool;
use priority_queue::PriorityQueue;


#[derive(Clone)]
pub struct Config {
    socket_address: String,
    worker_threads: usize
}

impl Config {
    pub fn new(mut args: env::Args) -> Result<Config, &'static str> {
        args.next();

        let socket_address = match args.next() {
            Some(arg) => arg,
            None => return Err("No socket address specified")
        };

        let worker_threads = match args.next() {
            Some(worker_threads) => {
                match usize::from_str_radix(worker_threads.as_str(), 10) {
                    Ok(worker_threads) => worker_threads,
                    Err(_) => return Err("Could not parse number of worker threads")
                }
            },
            None => 10
        };

        Ok(Config { socket_address, worker_threads })
    }

    pub fn socket_address(&self) -> &str {
        self.socket_address.as_str()
    }
}

#[derive(Debug)]
struct LockError;
impl Error for LockError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        // Generic error, underlying cause isn't tracked.
        None
    }
}
impl fmt::Display for LockError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "lock could not be acquired")
    }
}

struct Repository {
    watched_map: HashMap<String, Entry>,
    removal_queue: PriorityQueue<String, cmp::Reverse<SystemTime>>,
    max_capacity: usize
}

impl Repository {
    fn new() -> Self {
        return Self::with_capacity(1000);
    }

    fn with_capacity(capacity: usize) -> Self {
        let watched_map = HashMap::new();
        let removal_queue = PriorityQueue::new();
        Self { watched_map, removal_queue, max_capacity: capacity }
    }

    fn get_entry(&mut self, path: &str) -> &Entry {
        let last_modified = match fs::metadata(path) {
            Ok(metadata) => match metadata.modified() {
                Ok(modified) => Some(modified),
                Err(_) => None
            },
            Err(_) => None
        };

        let entry = self.watched_map.get(path);
        // there are several cases where we want to try to (re)read the watched information:
        // - if we have no entry yet for the given path
        // - if the file under `path` exists now but we have an entry that was created when it did
        //   not, i.e. entry.modified_time is not set
        // - if the file under `path` exists and was modified after we created the associated entry
        // - if we created the entry when the file under `path` existed but it does not exist now
        if entry.is_none() // there is no entry at all
            || last_modified.is_some() && ( // the file under path exists
                entry.unwrap().modified_time.is_none() || // the entry was created when it did not
                entry.unwrap().modified_time.unwrap() < last_modified.unwrap() // it has been modified
            )
            || last_modified.is_none() && // the file under path does not exist
                entry.is_some() && // but we still have an entry for it
                entry.unwrap().modified_time.is_some() { // that was created when the file existed
            let entry = create_entry(path, last_modified);
            // we need to make sure we don't exceed the allowed max capacity, we do this by
            // removing the oldest elements until we have enough space for the key we want to add
            let len = self.watched_map.len() + if !self.watched_map.contains_key(path) { 1 } else { 0 };
            let mut to_remove = len.checked_sub(self.max_capacity).unwrap_or(0);
            while to_remove > 0 {
                if let Some((key, _)) = self.removal_queue.pop() {
                    self.watched_map.remove(key.as_str());
                }
                to_remove -= 1;
            }
            // now we can insert the new entry
            self.watched_map.insert(path.to_string(), entry);
            // implement a FIFO removal pattern for now
            self.removal_queue.push(path.to_string(), cmp::Reverse(time::SystemTime::now()));
        }

        self.watched_map.get(path).unwrap()
    }
}

fn create_entry(path: &str, modified_time: Option<SystemTime>) -> Entry {
    println!("Worker {:?} attempting to read file \"{}\"", thread::current().id(), path);
    let mut watched_list = HashSet::new();
    if let Ok(file) = fs::File::open(path) {
        let file = BufReader::new(file);
        for line in file.lines() {
            if let Ok(line) = line {
                watched_list.insert(line);
            }
        }
    }
    Entry { watched_list, modified_time }
}

#[derive(Debug)]
struct Entry {
    watched_list: HashSet<String>,
    modified_time: Option<SystemTime>
}

impl Entry {
    fn is_watched(&self, path: &str) -> bool {
        return self.watched_list.contains(path);
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    println!("Starting {} worker threads", config.worker_threads);

    let pool = ThreadPool::new(config.worker_threads);
    let repo = Arc::new(Mutex::new(Repository::new()));
    let listener = UnixListener::bind(config.socket_address())?;

    for stream in listener.incoming() {
        let stream = stream?;
        let repo = Arc::clone(&repo);
        pool.execute(move || {
            println!("Worker {:?} started connection handling", thread::current().id());
            match handle_client(stream, repo) {
                Ok(_) => println!("Worker {:?} finished connection handling", thread::current().id()),
                Err(e) => println!("Worker {:?} finished prematurely because of error: {}", thread::current().id(), e)
            }
        });
    }

    Ok(())
}

pub fn clean_up(config: &Config) -> Result<(), Box<dyn Error>> {
    fs::remove_file(config.socket_address.as_str())?;
    Ok(())
}

fn handle_client(stream: UnixStream, repo: Arc<Mutex<Repository>>) -> Result<(), Box<dyn Error>> {
    let mut stream = BufStream::new(stream);
    let mut path_key = String::new();
    loop {
        match stream.read_line(&mut path_key) {
            Ok(result) => {
                if result == 0 {
                    return Ok(());
                }
                path_key = path_key.trim().to_string();
            },
            Err(_) => path_key.clear()
        }
        if !path_key.is_empty() {
            write_output(&mut stream, b"Ok", true)?;
            break;
        } else {
            write_output(&mut stream, b"Error", true)?;
        }
    }
    let mut line: String = String::new();
    let mut file_list = Vec::new();
    loop {
        loop {
            match stream.read_line(&mut line) {
                Ok(result) => {
                    if result == 0 {
                        return Ok(());
                    }
                    let string = line.trim().to_string();
                    // we stop taking input on a blank line
                    if string.is_empty() {
                        break;
                    }
                    file_list.push(Some(string))
                },
                Err(_) => file_list.push(None)
            }
            line.clear();
        }

        // if we don't have any files to check we don't need to do anything
        if file_list.is_empty() {
            continue;
        }

        // otherwise we need to get the appropriate entry
        let mut repo = match repo.lock() {
            Ok(repo) => repo,
            Err(_) => return Err(Box::new(LockError{}))
        };
        let entry = repo.get_entry(path_key.as_str());

        for file in &file_list {
            match file {
                Some(file) => {
                    if entry.is_watched(file.as_str()) {
                        write_output(&mut stream, b"Yes", false)?;
                    } else {
                        write_output(&mut stream, b"No", false)?;
                    }
                },
                None => write_output(&mut stream, b"Error", false)?
            }
        }
        stream.flush()?;
        file_list.clear();
    }
}

fn write_output<T: Write>(writer: &mut T, content: &[u8], do_flush: bool) -> Result<(), Box<dyn Error>> {
    writer.write(content)?;
    writer.write(b"\n")?;
    if do_flush {
        writer.flush()?;
    }
    Ok(())
}
