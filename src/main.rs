use std::{env, process, thread};
use std::error::Error;
use signal_hook::iterator::Signals;
use watchd::Config;


const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

fn main() {
    eprintln!("watchd v{}", VERSION.unwrap_or("?"));

    let config = Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = setup_signal_handlers(&config) {
        eprintln!("Problem setting up signal handlers: {}", e);
        process::exit(1);
    }

    if let Err(e) = watchd::run(config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}

fn setup_signal_handlers(config: &Config) -> Result<(), Box<dyn Error>> {
    let signals = Signals::new(&[signal_hook::SIGINT, signal_hook::SIGTERM])?;
    let config = config.clone();
    thread::spawn(move || {
        for signal in &signals {
            match signal {
                signal_hook::SIGINT | signal_hook::SIGTERM => {
                    eprintln!("Terminating, performing cleanup");
                    match watchd::clean_up(&config) {
                        Ok(_) => process::exit(0),
                        Err(e) => {
                            eprintln!("Error performing cleanup: {}", e);
                            process::exit(1)
                        }
                    }
                }
                _ => unreachable!()
            }
        }
    });
    Ok(())
}
